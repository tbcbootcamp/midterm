package ge.tbc.midterm.model


class RecyclerModel (
    val currency: String = "",
    val rate: Double = 0.0,
    val difference: Double = 0.0
)