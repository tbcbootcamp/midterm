package ge.tbc.midterm.model

data class CurrencyResponseModel (
    val currency : String = "",
    val currencyName: String = "",
    val centerRateCbr: Double = 0.0,
    val centerRateDifferenceCbr: Double = 0.0,
    val centerRateTbcr: Double? = null,
    val centerRateDifferenceTbcr: Double? = null,
    val buyRateTbcr: Double? = null,
    val sellRateTbcr: Double? = null,
    val dateCbr: Long = 0,
    val dateTbcr: Long = 0,
    val refCurrencyId: Int = 0
)