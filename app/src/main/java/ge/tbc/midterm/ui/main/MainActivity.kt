package ge.tbc.midterm.ui.main

import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import ge.tbc.midterm.*
import ge.tbc.midterm.model.CurrencyResponseModel
import ge.tbc.midterm.model.RecyclerModel
import ge.tbc.midterm.network.CustomCallback
import ge.tbc.midterm.network.DataLoader
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val items = ArrayList<RecyclerModel>()
    private lateinit var adapter: CurrencyAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        loadData()
    }

    private fun init() {

        currencyRecyclerViewRefreshLayout.setOnRefreshListener {
            loadData()
            amountEditText.setText("")
            currencyRecyclerViewRefreshLayout.isRefreshing = false
        }

        convertButton.setOnClickListener {
            val amount = amountEditText.text.toString()
            if (amount.isNotEmpty()) {
                val lariInt = amount.toDouble()
                loadData(lariInt)
            } else {
                Toast.makeText(this, "Please enter amount", Toast.LENGTH_SHORT).show()
            }
        }

        currencyRecyclerView.layoutManager = LinearLayoutManager(this)
        adapter = CurrencyAdapter(items)
        currencyRecyclerView.adapter = adapter
    }

    private fun loadData(baseAmount: Double = 1.0) {
        DataLoader.getRequest(
            DataLoader.RATES,
            object : CustomCallback {
                override fun onSuccess(response: String, message: String) {

                    val data: Array<CurrencyResponseModel> =
                        Gson().fromJson(
                            response,
                            Array<CurrencyResponseModel>::class.java
                        )

                    items.clear()
                    for (i in data) {
                        items.add(
                            RecyclerModel(
                                i.currency,
                                i.centerRateCbr * baseAmount,
                                i.centerRateDifferenceCbr
                            )
                        )
                    }
                    adapter.notifyDataSetChanged()
                }

                override fun onError(response: String, message: String) {
                    d("serverResponse", message)
                }

                override fun onFailure(response: String) {
                    d("serverResponse", response)
                }

            })
    }

}