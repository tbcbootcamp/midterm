package ge.tbc.midterm.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ge.tbc.midterm.R
import ge.tbc.midterm.model.RecyclerModel
import kotlinx.android.synthetic.main.currency_item.view.*
import java.math.BigDecimal
import java.math.RoundingMode

class CurrencyAdapter(private val items: ArrayList<RecyclerModel>) :
    RecyclerView.Adapter<CurrencyAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.currency_item,
            parent,
            false
        )
    )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var model: RecyclerModel

        fun onBind() {
            model = items[adapterPosition]
            itemView.currencyTextView.text = model.currency
            itemView.rateTextView.text = BigDecimal(model.rate).setScale(2, RoundingMode.HALF_EVEN).toString()
            itemView.differenceTextView.text = model.difference.toString()
        }

    }

}