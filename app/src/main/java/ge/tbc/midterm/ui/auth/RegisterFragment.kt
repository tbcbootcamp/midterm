package ge.tbc.midterm.ui.auth

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import ge.tbc.midterm.ui.main.MainActivity
import ge.tbc.midterm.R
import kotlinx.android.synthetic.main.fragment_register.*

class RegisterFragment : Fragment() {

    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        auth = FirebaseAuth.getInstance()

        signUpButton.setOnClickListener {

            val email = emailField.text.toString()
            val password = passwordField.text.toString()


            if (email.isNotEmpty() && password.isNotEmpty()) {


                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(
                                context,
                                "რეგისტრაცია წარმატებით დასრულდა",
                                Toast.LENGTH_SHORT
                            ).show()


                            val intent = Intent(context, MainActivity::class.java)
                            startActivity(intent)


                        } else {
                            Toast.makeText(
                                context, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            } else {
                Toast.makeText(context, "შეავსეთ ყველა მოცემული ველი", Toast.LENGTH_SHORT).show()
            }
        }
        signInTextView.setOnClickListener {
            val transaction = activity!!.supportFragmentManager.beginTransaction()
            transaction.replace(
                R.id.baseContainer,
                LoginFragment(), "LoginFragment")
            transaction.addToBackStack("LoginFragment")
            transaction.commit()
        }
    }
}